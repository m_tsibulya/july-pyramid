from copy import deepcopy
import types
import pony
import six
import json
from pony.orm import desc
from pony.orm.core import Entity, Set
from pony.orm.serialization import to_dict
from pyramid.httpexceptions import (
    HTTPMethodNotAllowed, HTTPUnauthorized,
    HTTPOk, HTTPAccepted, HTTPCreated,
    HTTPNoContent, HTTPBadRequest
)
from pyramid.response import Response

# from july.conf import settings
from july.db import get_fields, quote_value_for_eval, check_set
from july.db.converters.file.file import WrappedStoredFile
from july.forms import ModelForm
from .authorization import ReadOnlyAuthorization
from .pagination import SimplePagination, PonyPagination
from .serializers import MultipartJSONSerializer
from .validation import FormValidation
from .cache import cache


class CoreResource(object):
    pass


class ResourceOptions(object):
    """
    A configuration class for ``Resource``.
    Provides sane defaults and the logic needed to augment these settings with
    the internal ``class Meta`` used on ``Resource`` subclasses.
    """
    serializer = MultipartJSONSerializer()
    authorization = ReadOnlyAuthorization()
    authentication = None
    pagination = SimplePagination()
    # authorization = ReadOnlyAuthorization()
    # cache = NoCache()
    # throttle = BaseThrottle()
    logger = None
    validation = None
    # paginator_class = Paginator

    allowed_methods = ['get', 'post', 'put', 'delete',]

    list_allowed_methods = None
    detail_allowed_methods = None

    limit = 100 #getattr(settings, 'API_LIMIT_PER_PAGE', 100)
    max_limit = 1000

    api_name = None
    resource_name = None
    urlconf_namespace = None
    default_format = 'application/json'
    filtering = []
    ordering = []
    object_class = None
    queryset = None
    fields = None
    excludes = []
    include_resource_uri = True
    include_absolute_url = False
    always_return_data = False
    collection_name = 'objects'
    detail_uri_name = 'pk'

    def __new__(cls, meta=None):
        overrides = {}

        # Handle overrides.
        if meta:
            for override_name in dir(meta):
                # No internals please.
                if not override_name.startswith('_'):
                    overrides[override_name] = getattr(meta, override_name)

        allowed_methods = overrides.get('allowed_methods', ['get', 'post', 'put', 'delete', 'patch'])

        if overrides.get('list_allowed_methods', None) is None:
            overrides['list_allowed_methods'] = allowed_methods

        if overrides.get('detail_allowed_methods', None) is None:
            overrides['detail_allowed_methods'] = allowed_methods

        if six.PY3:
            return object.__new__(type('ResourceOptions', (cls,), overrides))
        else:
            return object.__new__(type(b'ResourceOptions', (cls,), overrides))


class DeclarativeMetaclass(type):
    def __new__(cls, name, bases, attrs):
        attrs['base_fields'] = {}
        declared_fields = {}

        # Inherit any fields from parent(s).
        parents = [b for b in bases if issubclass(b, CoreResource)]
        # Simulate the MRO.
        parents.reverse()

        for p in parents:
            parent_fields = getattr(p, 'base_fields', {})

            for field_name, field_object in parent_fields.items():
                attrs['base_fields'][field_name] = deepcopy(field_object)

        for field_name, obj in attrs.copy().items():
            # Look for ``dehydrated_type`` instead of doing ``isinstance``,
            # which can break down if Tastypie is re-namespaced as something
            # else.
            if hasattr(obj, 'dehydrated_type'):
                field = attrs.pop(field_name)
                declared_fields[field_name] = field

        attrs['base_fields'].update(declared_fields)
        attrs['declared_fields'] = declared_fields
        new_class = super(DeclarativeMetaclass, cls).__new__(cls, name, bases, attrs)
        opts = getattr(new_class, 'Meta', None)
        new_class._meta = ResourceOptions(opts)

        if not getattr(new_class._meta, 'resource_name', None):
            # No ``resource_name`` provided. Attempt to auto-name the resource.
            class_name = new_class.__name__
            name_bits = [bit for bit in class_name.split('Resource') if bit]
            resource_name = ''.join(name_bits).lower()
            new_class._meta.resource_name = resource_name

        # if getattr(new_class._meta, 'include_resource_uri', True):
        #     if 'resource_uri' not in new_class.base_fields:
        #         new_class.base_fields['resource_uri'] = fields.CharField(readonly=True, verbose_name="resource uri")
        # elif 'resource_uri' in new_class.base_fields and 'resource_uri' not in attrs:
        #     del(new_class.base_fields['resource_uri'])

        for field_name, field_object in new_class.base_fields.items():
            if hasattr(field_object, 'contribute_to_class'):
                field_object.contribute_to_class(new_class, field_name)

        return new_class

# from restless.constants import *


class Resource(six.with_metaclass(DeclarativeMetaclass, CoreResource)):
    status_map = {
        'list_get': HTTPOk.code,
        'list_update': HTTPAccepted.code,
        'list_create': HTTPCreated.code,
        'list_delete': HTTPNoContent.code,

        'detail_get': HTTPOk.code,
        'detail_update': HTTPAccepted.code,
        'detail_create': HTTPCreated.code,
        'detail_delete': HTTPNoContent.code,
    }

    def __init__(self, request):
        self.request = request
        self.__dehydate_methods__ = {
            method_name.replace('dehydrate_', ''): getattr(self, method_name)
            for method_name in dir(self)
            if callable(getattr(self, method_name)) and
               method_name.startswith('dehydrate_')
        }
        self.__hydate_methods__ = {
            method_name.replace('hydrate_', ''): getattr(self, method_name)
            for method_name in dir(self)
            if callable(getattr(self, method_name)) and
               method_name.startswith('hydrate_')
        }

    def build_response(self, data, headers, status=HTTPOk.code):
        if status == HTTPNoContent.code:
            # Avoid crashing the client when it tries to parse nonexisting JSON.
            content_type = 'text/plain'
        else:
            content_type = 'application/json'

        resp = Response(
            data, status_code=status,
            content_type=content_type, charset='utf-8', headerlist=headers)
        return resp

    def build_error(self, errors, e=None, code=HTTPBadRequest.code):
        resp = Response(json={
            'errors': errors
        }, status_code=code)
        return resp

    def authenticate(self, *args, **kwargs):
        if self._meta.authentication:
            return self._meta.authentication.get_user(self.request)
        return None

    def is_authorized(self, method, *args, **kwargs):
        check_method = getattr(self, 'can_%s' % method, getattr(self._meta.authorization, 'can_%s' % method))
        # self.authenticate() WTF
        if check_method:
            return check_method(
                self.request,
                getattr(self._meta, 'object_class', None),
                *args, **kwargs
            )
        return False

    def validate_data(self, method, data):
        if not self._meta.validation or method not in ('POST', 'PUT'):
            return []

        return self._meta.validation.validate(data)

    def prepare(self, data):
        if isinstance(data, dict):
            return self.__dehydrate__(data)
        else:
            return [self.__dehydrate__(obj) for obj in data]

    def deserialize(self, method, endpoint):
        """
        A convenience method for deserializing the body of a request.

        If called on a list-style endpoint, this calls ``deserialize_list``.
        Otherwise, it will call ``deserialize_detail``.

        :param method: The HTTP method of the current request
        :type method: string

        :param endpoint: The endpoint style (``list`` or ``detail``)
        :type endpoint: string

        :param body: The body of the current request
        :type body: string

        :returns: The deserialized data
        :rtype: ``list`` or ``dict``
        """
        if endpoint == 'list':
            return self.deserialize_list()

        return self.deserialize_detail()

    def deserialize_list(self):
        """
        Given a string of text, deserializes a (presumed) list out of the body.

        :param body: The body of the current request
        :type body: string

        :returns: The deserialized body or an empty ``list``
        """
        if self.request.body:
            return self._meta.serializer.deserialize(self.request)

        return []

    def deserialize_detail(self):
        """
        Given a string of text, deserializes a (presumed) object out of the body.

        :param body: The body of the current request
        :type body: string

        :returns: The deserialized body or an empty ``dict``
        """
        if self.request.body:
            return self._meta.serializer.deserialize(self.request)

        return {}

    def serialize(self, method, endpoint, data):
        """
        A convenience method for serializing data for a response.

        If called on a list-style endpoint, this calls ``serialize_list``.
        Otherwise, it will call ``serialize_detail``.

        :param method: The HTTP method of the current request
        :type method: string

        :param endpoint: The endpoint style (``list`` or ``detail``)
        :type endpoint: string

        :param data: The body for the response
        :type data: object

        :returns: A serialized version of the data
        :rtype: string
        """
        if endpoint == 'list':
            # Create is a special-case, because you POST it to the collection,
            # not to a detail.
            if method == 'POST':
                return self.serialize_detail(data)
            return self.serialize_list(data)
        return self.serialize_detail(data)

    def serialize_list(self, data):
        """
        Given a collection of data (``objects`` or ``dicts``), serializes them.

        :param data: The collection of items to serialize
        :type data: list or iterable

        :returns: The serialized body
        :rtype: string
        """
        if data is None:
            return ''

        # Check for a ``Data``-like object. We should assume ``True`` (all
        # data gets prepared) unless it's explicitly marked as not.
        # if not getattr(data, 'should_prepare', True):
        #     prepped_data = data.value
        # else:
        #     prepped_data = [self.prepare(item) for item in data]
        # final_data = self.wrap_list_response(prepped_data)
        return self._meta.serializer.serialize(self.prepare(data))

    def serialize_detail(self, data):
        """
        Given a single item (``object`` or ``dict``), serializes it.

        :param data: The item to serialize
        :type data: object or dict

        :returns: The serialized body
        :rtype: string
        """
        if data is None:
            return ''

        # Check for a ``Data``-like object. We should assume ``True`` (all
        # data gets prepared) unless it's explicitly marked as not.
        # if not getattr(data, 'should_prepare', True):
        #     prepped_data = data.value
        # else:
        #     prepped_data = self.prepare(data)
        return self._meta.serializer.serialize(self.prepare(data))

    def wrap_list_response(self, data):
        """
        Takes a list of data & wraps it in a dictionary (within the ``objects``
        key).

        For security in JSON responses, it's better to wrap the list results in
        an ``object`` (due to the way the ``Array`` constructor can be attacked
        in Javascript). See http://haacked.com/archive/2009/06/25/json-hijacking.aspx/
        & similar for details.

        Overridable to allow for modifying the key names, adding data (or just
        insecurely return a plain old list if that's your thing).

        :param data: A list of data about to be serialized
        :type data: list

        :returns: A wrapping dict
        :rtype: dict
        """
        return data

    def get(self):
        return self.handle('detail', 'GET', self.request.matchdict['id'])

    def post(self):
        return self.handle('detail', 'POST', self.request.matchdict['id'])

    def put(self):
        return self.handle('detail', 'PUT', self.request.matchdict['id'])

    def delete(self):
        return self.handle('detail', 'DELETE', self.request.matchdict['id'])

    def collection_get(self):
        return self.handle('list', 'GET')

    def collection_post(self):
        return self.handle('list', 'POST')

    def collection_put(self):
        return self.handle('list', 'PUT')

    def collection_delete(self):
        return self.handle('list', 'DELETE')

    def list_get(self, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def list_post(self, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def list_put(self, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def list_delete(self, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def detail_get(self, pk, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def detail_post(self, pk, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def detail_put(self, pk, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def detail_delete(self, pk, data, authorization=None):
        raise NotImplementedError("Unsupported method")

    def search(self, qs):
        return qs

    def validate(self):
        return self.build_response('{}', [])

    def __get_files__(self):
        return {}

    def __get_sets__(self, data):
        result = {}
        for k in data:
            if k in self.__hydate_methods__:
                continue

            attr = getattr(self._meta.object_class, k, None)
            if attr and isinstance(attr, Set):
                query = 'lambda x: x.%s in data[k]' % attr.py_type._pk_.name
                result[k] = attr.py_type.select(eval(query))

        return result


    def __get_data__(self, method, endpoint):
        data = self.deserialize(method, endpoint)
        if self.request.POST:
            data.update(self.__get_files__())
            data.update(self.__get_sets__(data))
        return data

    def __get_headers__(self, *args, **kwargs):
        start = kwargs['start']
        end = kwargs['end']
        length = kwargs['length']
        headers = []

        if 'length' in kwargs:
            headers.append(('X-Total-Count', str(length)))
        if 'length' in kwargs and \
            'start' in kwargs and \
            'end' in kwargs:
            headers.append(('X-Content-Range', '%s-%s/%s' % (start, end, length)))

        return headers

    def __paginate__(self, data, start, end):
        return data[start:end]

    def __hydrate__(self, data):
        if self.request.POST:
            data.update({
                k: v(data.get(k))
                for k, v in self.__hydate_methods__.items()
            })
        return data

    def __dehydrate__(self, data, obj=None):
        data.update({
            k: v(obj or data)
            for k, v in self.__dehydate_methods__.items()
        })
        return data

    @cache
    def get_serialized(self, endpoint, method, *args, **kwargs):
        self.endpoint = endpoint
        self.method = method

        if method.lower() not in getattr(self._meta, '%s_allowed_methods' % endpoint):
            return {
                'data': '[]',
                'headers': [],
                'status': HTTPMethodNotAllowed.code
            }

        view_method = getattr(self, '%s_%s' % (endpoint, method.lower()))
        authorization = self.is_authorized(view_method.__name__, *args, **kwargs)
        if not authorization:
            return {
                'data': '[]',
                'headers': [],
                'status': HTTPUnauthorized.code
            }

        data = self.__get_data__(method, endpoint)
        errors = self.validate_data(method, data)
        data = self.__hydrate__(data)
        if errors:
            return {
                'data': json.dumps({
                    'errors': errors
                }),
                'headers': [],
                'status': HTTPBadRequest.code
            }
        data = view_method(data=data, authorization=authorization, *args, **kwargs)
        data = list(data) if isinstance(data, types.GeneratorType) else data
        length = None
        start = None
        end = None

        if endpoint == 'list' and method == 'GET':
            length = self._meta.pagination.__get_length__(data)
            start, end = self._meta.pagination.__get_crop__(
                start=self.request.GET.get('_start'),
                end=self.request.GET.get('_end'),
                length=length,
                limit=self._meta.limit,
                max_limit=self._meta.max_limit
            )
            data = self._meta.pagination(data, start, end)
        headers = self.__get_headers__(length=length, start=start, end=end)
        serialized = self.serialize(method, endpoint, data)
        status = self.status_map.get(view_method.__name__, HTTPOk.code)

        return {
            'headers': headers,
            'data': serialized,
            'status': status
        }

    def handle(self, endpoint, method, *args, **kwargs):
        """
        A convenient dispatching method, this centralized some of the common
        flow of the views.

        This wraps/calls the methods the user defines (``list/detail/create``
        etc.), allowing the user to ignore the
        authentication/deserialization/serialization/response & just focus on
        their data/interactions.

        :param endpoint: The style of URI call (typically either ``list`` or
            ``detail``).
        :type endpoint: string

        :param args: (Optional) Any positional URI parameter data is passed
            along here. Somewhat framework/URL-specific.

        :param kwargs: (Optional) Any keyword/named URI parameter data is
            passed along here. Somewhat framework/URL-specific.

        :returns: A response object
        """
        # try:
        data = self.get_serialized(endpoint, method, *args, **kwargs)
        return self.build_response(**data)
        # except Exception as err:
        #      return self.build_error(['Unexpected error'], err)


class ModelDeclarativeMetaclass(DeclarativeMetaclass):
    def __new__(cls, name, bases, attrs):
        meta = attrs.get('Meta')
        # Sanity check: ModelResource needs either a queryset or object_class:
        # if meta and not hasattr(meta, 'queryset') and not hasattr(meta, 'object_class'):
        #     msg = "ModelResource (%s) requires Meta.object_class or Meta.queryset"
        #     raise ImproperlyConfigured(msg % name)

        new_class = super(ModelDeclarativeMetaclass, cls).__new__(cls, name, bases, attrs)
        specified_fields = getattr(meta, 'fields', [])
        excludes = getattr(meta, 'excludes', [])
        field_names = list(new_class.base_fields.keys())

        include_fields = dict(zip(specified_fields,specified_fields))

        # if include_fields is None:
        #     if meta and meta.object_class:
        #         include_fields = dict(zip(specified_fields,specified_fields))
        #     else:
        #         include_fields = {}

        for field_name in field_names:
            if field_name == 'resource_uri':
                continue
            if field_name in new_class.declared_fields:
                continue
            if specified_fields is not None and field_name not in include_fields:
                del(new_class.base_fields[field_name])
            if field_name in excludes:
                del(new_class.base_fields[field_name])

        # Add in the new fields.
        new_class.base_fields.update(new_class.get_fields(include_fields, excludes))

        if meta and meta.object_class and not getattr(meta, 'validation', None):
            _form_meta = type('Meta', (object,), {'object_class': meta.object_class})
            _form_class = type('ModelFormClass', (ModelForm,), {'Meta': _form_meta})
            new_class._meta.validation = FormValidation(_form_class)

        new_class._meta.pagination = PonyPagination()
        # new_class.preparer = FieldsPreparer(fields=new_class.base_fields)

        # if getattr(new_class._meta, 'include_absolute_url', True):
        #     if 'absolute_url' not in new_class.base_fields:
        #         new_class.base_fields['absolute_url'] = fields.CharField(attribute='get_absolute_url', readonly=True)
        # elif 'absolute_url' in new_class.base_fields and 'absolute_url' not in attrs:
        #     del(new_class.base_fields['absolute_url'])

        return new_class


class BaseModelResource(Resource):
    def query(self):
        return self._meta.object_class.select()

    def get_queryset(self):
        queryset = self.query()
        _sort = (set([self.request.GET.get('_sort')]) & set(self._meta.ordering) or self._meta.ordering or ['id'])[0]
        _order = self.request.GET.get('_order')
        _q = self.request.GET.get('q')

        _filter = {
            X: quote_value_for_eval(self.request.GET[X]) for X in
            set(self.request.GET.keys()) & set(self._meta.filtering)
        }

        if _filter:
            _qset ={K for K in _filter.keys() if check_set(K, self._meta.object_class)}
            # filter exist (sets only)
            _q1k = {K for K, V in _filter.items() if V == '"*"' and K in _qset}
            _q1 = ['len(X.%s) > 0' % (K,) for K, V in _filter.items() if K in _q1k]
            # filter exist (any other)
            _q2k = {K for K, V in _filter.items() if V == '"*"' and K not in _qset}
            _q2 = ['X.%s is not None' % (K,) for K, V in _filter.items() if K in _q2k]
            # filter set occurences
            _q3k = _qset - _q1k
            _q3 = [' or '.join(['%s in X.%s' % (VV, K) for VV in V]) for K, V in _filter.items() if K in _q3k]
            # filter bool
            _q4k = {K for K, V in _filter.items() if V in ('"true"', '"false"')}
            _q4 = ['X.%s is %s' % (K, json.loads(V.strip('"'))) for K, V in _filter.items() if K in _q4k]
            # filter other
            _q5 = ['X.%s %s %s' % (K, '==' if not isinstance(V, list) else 'in', V) for K, V in _filter.items() if K not in _q1k | _q2k | _q3k | _q4k]
            _query = ' and '.join(set(_q1) | set(_q2) | set(_q3) | set(_q4) | set(_q5))
            queryset = queryset.filter(eval('lambda X: ' + _query))

        for search_name in filter(lambda X: X.startswith('_q'), self.request.GET.keys()):
            attname = ('search_%s' % search_name.strip('_q').strip('_')).strip('_')
            method = getattr(self, attname)
            if method:
                queryset = method(queryset, self.request.GET.get(search_name))

        if _q:
            queryset = self.search(queryset, _q)

        if _sort and _sort.lstrip('-') in self.base_fields:
            sort = (lambda x: desc(getattr(x, _sort.strip('-')))) \
                if _order == 'DESC' or _sort.startswith('-') else lambda x: getattr(x, _sort)
            queryset = queryset.order_by(sort)

        return queryset

    def search(self, qs, query):
        if not self._meta.searching:
            return qs

        _query = ' or '.join(['"%s" in X.%s' % (query, K) for K in self._meta.searching])
        return qs.filter(eval('lambda X: ' + _query))

    def get_object(self, pk):
        return self._meta.object_class.get(id=pk)

    def list_get(self, data, authorization=None):
        return self.get_queryset()

    def list_post(self, data, authorization=None):
        obj = self._meta.object_class(**data)
        return obj

    def list_put(self, data, authorization=None):
        pass

    def list_delete(self, data, authorization=None):
        self.get_queryset().delete(bulk=True)

    def detail_get(self, pk, data, authorization=None):
        return self.get_object(pk=pk)

    def detail_post(self, pk, data, authorization=None):
        pass

    def detail_put(self, pk, data, authorization=None):
        obj = self.get_object(pk=pk)
        self._meta.logger and self._meta.logger.store(
            user=getattr(authorization, 'user', None),
            obj=obj,
            data=data
        )
        obj.set(**data)
        return obj

    def detail_delete(self, pk, data, authorization=None):
        self.get_object(pk=pk).delete()

    # def is_authorized(self, method, *args, **kwargs):
    #     check_method = getattr(self._meta.authorization, 'can_%s' % method)
    #     if check_method:
    #         return check_method(self.request, self._meta.object_class, *args, **kwargs)
    #     return False

    def prepare(self, data):
        """
        Given an item (``object`` or ``dict``), this will potentially go through
        & reshape the output based on ``self.prepare_with`` object.

        :param data: An item to prepare for serialization
        :type data: object or dict

        :returns: A potentially reshaped dict
        :rtype: dict
        """
        # object_attributes = (getattr(self._meta.object_class, K) for K in self.base_fields)
        object_attributes = self.base_fields.values()

        if isinstance(data, Entity):
            return self.__dehydrate__(data.to_dict(only=object_attributes, with_collections=True, related_objects=True), data)

        # TODO
        # object_attributes = [not X.startswith('_') for X in object_attributes]
        return [self.__dehydrate__(obj.to_dict(only=object_attributes, with_collections=True, related_objects=True), obj) for obj in data]

    def __get_files__(self):
        files = {
            K: WrappedStoredFile(
                storage=getattr(self._meta.object_class, K).converters[0].storage,
                file=V.file,
                filename=V.filename
            )
            for K, V in self.request.POST.items()
            if hasattr(V, 'file')
        }

        return files

    def __get_length__(self, data):
        return data.count()

    @pony.orm.db_session
    def handle(self, endpoint, *args, **kwargs):
        return super(BaseModelResource, self).handle(endpoint, *args, **kwargs)

    @classmethod
    def get_fields(cls, include=None, excludes=None):
        final_fields = {}
        include = include or []

        if not cls._meta.object_class:
            return {}

        attributes = dict(get_fields(cls._meta.object_class)).keys()
        excludes = excludes or []

        for f in attributes:
            if f.startswith('_'):
                continue

            if f in cls.base_fields:
                continue

            if f in excludes:
                continue

            if include and f not in include:
                continue

            final_fields[f] = f
        return final_fields


class ModelResource(six.with_metaclass(ModelDeclarativeMetaclass, BaseModelResource)):
    pass

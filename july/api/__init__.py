"""
API module based on cornice
https://cornice.readthedocs.io/en/latest/

and inspired by restless and tastypie
https://github.com/toastdriven/restless
https://github.com/django-tastypie/django-tastypie

"""
import urllib
from cornice import Service
from cornice import resource as cornice_resource
from .cors import cors_policy
from .search import UnionSearch, SearchResource

def add_api_class(
    config,
    resource, resource_name,
    prefix=None, path=None,
    collection_path=None,
    collection_pattern='{id:\d+}',
    cors_policy=cors_policy,
    urls=None
):
    prefix = prefix or config.registry.settings.get('api_prefix', '/api/')
    collection_path = collection_path or urllib.parse.urljoin(
        prefix, resource_name.strip('/')
    )
    path = path or urllib.parse.urljoin(
        '%s/' % collection_path, collection_pattern
    )
    urls = list(urls) if urls else []
    _resource = cornice_resource.add_resource(
        resource,
        collection_path=collection_path,
        path=path,
        cors_policy=cors_policy
    )
    config.add_cornice_resource(_resource)

    if not urls:
        return

    for url in urls:
        method = None
        request_method = 'GET'
        if type(url) in (list, tuple):
            if len(url) == 2:
                url, request_method = url
            elif len(url) == 3:
                url, request_method, method = url

        method = method or url.split('/')[0]
        _service = Service(
            name='%s_%s' % (resource.__name__, method),
            path=urllib.parse.urljoin('%s/' % collection_path, url),
            cors_policy=cors_policy
        )
        _service.add_view(
            request_method,
            method,
            klass=resource
        )
        config.add_cornice_service(_service)


def add_api_search(
    config,
    database,
    *resources,
    search_resource=SearchResource,
    resource_name='search',
    prefix=None, path=None,
    cors_policy=cors_policy,
    min_length=2,
    keyword_name='_q'
):
    prefix = prefix or config.registry.settings.get('api_prefix', '/api/')
    collection_path = path or urllib.parse.urljoin(
        prefix, resource_name
    )
    _searcher = UnionSearch(database, *resources)
    _search_resource_meta = type('Meta', (object,), {
        'searcher': _searcher,
        'min_length': min_length,
        'keyword_name': keyword_name
    })
    _search_resource = type('SearchResource', (search_resource,), {
        'Meta': _search_resource_meta
    })

    # def _search(request):
    #     q = request.GET.get(keyword_name)
    #     if not q or len(q) < min_length:
    #         return {}
    #     return _searcher(q)
    config.add_api_resource(
        _search_resource, resource_name,
        prefix=prefix,
        collection_path=collection_path,
        path=urllib.parse.urljoin(
            '%s/' % collection_path, '{.+}'
        ),
        cors_policy=cors_policy
    )
    # cornice_resource.add_resource(
    #     _search_resource
    # )

    # _service = Service(
    #     name='search',
    #     path=path,
    #     cors_policy=cors_policy
    # )
    # _service.add_view('GET', _search)
    # config.add_cornice_service(_service)
    
    
def includeme(config):
    config.add_directive('add_api_resource', add_api_class)
    config.add_directive('add_api_search', add_api_search)

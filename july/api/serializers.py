import datetime
import decimal
import json
import uuid

from pony.orm.core import Entity
from pyramid.httpexceptions import HTTPBadRequest

from july.db.converters.file.file import WrappedStoredFile


class MoreTypesJSONEncoder(json.JSONEncoder):
    """
    A JSON encoder that allows for more common Python data types.

    In addition to the defaults handled by ``json``, this also supports:

        * ``datetime.datetime``
        * ``datetime.date``
        * ``datetime.time``
        * ``decimal.Decimal``
        * ``uuid.UUID``

    """
    def default(self, data):
        if isinstance(data, (datetime.datetime, datetime.date, datetime.time)):
            return data.isoformat()
        elif isinstance(data, decimal.Decimal) or isinstance(data, uuid.UUID):
            return str(data)
        elif isinstance(data, WrappedStoredFile):
            return data.to_dict() if data else ''
        elif isinstance(data, Entity):
            return data.id
        else:
            return super(MoreTypesJSONEncoder, self).default(data)


class Serializer(object):
    """
    A base serialization class.

    Defines the protocol expected of a serializer, but only raises
    ``NotImplementedError``.

    Either subclass this or provide an object with the same
    ``deserialize/serialize`` methods on it.
    """
    def deserialize(self, request):
        """
        Handles deserializing data coming from the user.

        Should return a plain Python data type (such as a dict or list)
        containing the data.

        :param body: The body of the current request
        :type body: string

        :returns: The deserialized data
        :rtype: ``list`` or ``dict``
        """
        raise NotImplementedError("Subclasses must implement this method.")

    def serialize(self, request):
        """
        Handles serializing data being sent to the user.

        Should return a plain Python string containing the serialized data
        in the appropriate format.

        :param data: The body for the response
        :type data: ``list`` or ``dict``

        :returns: A serialized version of the data
        :rtype: string
        """
        raise NotImplementedError("Subclasses must implement this method.")


class JSONSerializer(Serializer):
    def deserialize(self, request):
        """
        The low-level deserialization.

        Underpins ``deserialize``, ``deserialize_list`` &
        ``deserialize_detail``.

        Has no built-in smarts, simply loads the JSON.

        :param body: The body of the current request
        :type body: string

        :returns: The deserialized data
        :rtype: ``list`` or ``dict``
        """
        try:
            if isinstance(request.body, bytes):
                return json.loads(request.body.decode('utf-8'))
        except ValueError:
            raise HTTPBadRequest('Request body is not valid JSON')

    def serialize(self, data):
        """
        The low-level serialization.

        Underpins ``serialize``, ``serialize_list`` &
        ``serialize_detail``.

        Has no built-in smarts, simply dumps the JSON.

        :param data: The body for the response
        :type data: string

        :returns: A serialized version of the data
        :rtype: string
        """
        return json.dumps(data, cls=MoreTypesJSONEncoder)


class MultipartJSONSerializer(JSONSerializer):
    def deserialize(self, request):
        if request.headers.get('content-type').startswith('multipart/form-data'):
            if '_data' in request.POST:
                _data = json.loads(request.POST['_data'])
                del request.POST['_data']
                request.POST.update(_data)
            return request.POST
        return super(MultipartJSONSerializer, self).deserialize(request)

from july.utils import lazy
from pony.orm import db_session
from .resource import Resource


class UnionSearch(object):
    __resources__ = {}

    def __init__(self, database, *resources):
        self.database = database
        for resource in resources:
            self.append(resource)

    def append(self, resource):
        if resource._meta.searching:
            object_class = resource._meta.object_class
            fields = {attribute_name: getattr(object_class, attribute_name) for attribute_name in resource._meta.searching}
            columns = {getattr(field, 'column'): attribute_name for attribute_name, field in fields.items()}
            self.__resources__[resource._meta.object_class.__name__] = {
                'resource': resource,
                'object_class': object_class,
                'fields': fields,
                'columns': columns
            }

    @lazy
    def query(self):
        query = ' UNION '.join(
            ["(SELECT `%(pk)s`, '%(object_class)s' as __type__, %(display_name)s FROM `%(table)s` WHERE %(statement)s)" % {
                'pk': 'id',
                'display_name': 'CASE %s END as display_name' % ''.join(["WHEN `%(column)s` LIKE '{0}' THEN `%(column)s`" % {'column': column} for column in r['columns']]),
                'object_class': name,
                'table': r['object_class']._table_,
                'statement': ' OR '.join(["`%(column)s` LIKE '%(pattern)s'" % {
                    'column': field,
                    'pattern': '{0}'
                } for field in r['columns']])
            } for name, r in self.__resources__.items()
            ]
        )
        return '%s ORDER BY id' % query

    @lazy
    def count(self):
        return self.query.count("'%s'")

    def __call__(self, keyword):
        keyword = "%{0}%".format(keyword)
        cursor = self.database.execute(
            self.query.format(keyword)
        )
        data = cursor.fetchall()
        return [
            {
                'pk': pk,
                'type': kind,
                'display_name': val,

            } for pk, kind, val in data
        ]


class SearchResource(Resource):
    class Meta:
        searcher = None
        keyword_name = '_q'
        min_length = 2
        limit = 10
        max_limit = 20

    @db_session
    def list_get(self, data, authorization=None):
        q = self.request.GET.get(self._meta.keyword_name, None)
        if not self._meta.searcher or not q or len(q) < self._meta.min_length:
            return []

        return self._meta.searcher(q)

    @db_session
    def detail_get(self, pk, data, authorization=None):
        if not self._meta.searcher or not pk or len(pk) < self._meta.min_length:
            return []

        return self._meta.searcher(pk)

from uuid import UUID


class SessionAuthentication(object):
    def get_user(self, request, *args, **kwargs):
        return request.user


class TokenAuthentication(object):
    def __init__(self, token_class):
        self.token_class = token_class

    def get_user(self, request, *args, **kwargs):
        token_str = request.headers.get('X-Authorization')
        if not token_str:
            return None

        token = self.token_class.get(
            lambda x: x.token == UUID('{%s}' % token_str)
        )
        if token:
            return


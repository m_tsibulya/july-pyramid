#!/usr/bin/env python
# -*- coding: utf-8 -*-
import hashlib
import json
from july.cache import _cache
from pyramid.httpexceptions import HTTPOk


class RedisCache(object):
    def __init__(self, expire):
        self.expire = expire

    def set(self, key, value):
        _cache.set(key, value, self.expire)

    def get(self, key):
        return _cache.get(key)

    def key(self, *args):
        h = hashlib.md5()
        for arg in args[3:]:
            h.update(str(arg).encode('utf-8'))
        return ':'.join((args[0].lower(), args[1].lower(), args[2].lower(), h.hexdigest()))


def cache(f):
    def wrapped_f(self, endpoint, method, *args, **kwargs):
        caching = getattr(self._meta, 'caching', None)
        use = caching and method.lower() == 'get' and self.request.headers.get('X-Cache') != 'false'
        key = None
        key2 = None
        if use:
            key = caching.key(
                self.__class__.__name__,
                method,
                endpoint,
                *self.request.matchdict.keys(),
                *self.request.matchdict.values(),
                *self.request.GET.keys(),
                *self.request.GET.values(),
            )
            key2 = '%s:h' % key
            data = caching.get(key)
            headers = caching.get(key2)
            if data and headers:
                return {
                    'data': data,
                    'headers': json.loads(headers),
                    'status': HTTPOk.code
                }
        resp = f(self, endpoint, method, *args, **kwargs)
        if resp.get('status') == HTTPOk.code and use:
            caching.set(key, resp['data'])
            caching.set(key2, json.dumps(resp['headers']))
        return resp
    return wrapped_f

class FormValidation():
    form = None

    def __init__(self, form):
        self.form = form

    def validate(self, data):
        form = self.form(data)
        if not form.validate():
            return form.errors

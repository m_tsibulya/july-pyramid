from uuid import UUID, uuid4
from pony.orm import *
from july.compat.datetime import datetime, timedelta, now
from july.utils import lazy


class AbstractGroup(object):
    title = Required(str, unique=True)
    users = Set('User')

    def __unicode__(self):
        return self.title


class AbstractUser(object):
    """
    should be defined
    groups = Set('Group')
    """
    username = Required(str, unique=True)
    email = Optional(str)
    first_name = Optional(str)
    last_name = Optional(str)

    password = Required(str, default='!')
    is_superuser = Required(bool, default=False)
    is_staff = Required(bool, default=False)

    date_joined = Required(datetime, default=now)
    auth_backend = None

    @classmethod
    def authenticate(klass, request, user):
        return klass.auth_backend.authenticate(request, user)

    @classmethod
    def forget(klass, request):
        return klass.auth_backend.forget(request)

    def set_password(self, pw):
        self.password = self.auth_backend.hash_password(pw)
        return self

    def check_password(self, pw):
        return self.auth_backend.check_password(pw, self.password)

    @lazy
    def perms(self):
        if not hasattr(self, 'permissions'):
            return set([])

        classname = self.__class__.permissions.attrs[0].__name__
        return set(select(eval('p.name for p in %(perms_table)s if p.user == %(user_id)s or p.group.users in (%(user_id)s,)' % {
            'user_id': self.id,
            'perms_table': classname
        })))

    def has_perm(self, perm):
        return self.is_superuser or (self.is_staff and perm in self.perms)

    def has_perms(self, *perms):
        return self.is_superuser or (self.is_staff and not (set(perms) - self.perms))

    def get_token(self):
        if not hasattr(self, 'tokens'):
            return None

        try:
            return self.tokens.select(
                lambda x: x.date_valid > now()
            )[:][0]
        except IndexError:
            token = self.__class__.tokens.py_type(
                user=self
            )
            commit()
            return token

    def __unicode__(self):
        if self.first_name or self.last_name:
            return ' '.join(filter(bool, (self.first_name, self.last_name)))
        return self.username


class AbstractToken(object):
    TOKEN_VALID_DAYS = 7
    token = PrimaryKey(UUID, default=uuid4)
    user = Required('User')
    date_valid = Required(datetime, default=lambda: now() + timedelta(AbstractToken.TOKEN_VALID_DAYS))

    def is_valid(self):
        _now = now()
        is_valid = self.date_valid > _now
        if not is_valid:
            return False

        if self.date_valid - timedelta(int(self.TOKEN_VALID_DAYS/2)) < _now:
            self.set(
                date_valid=_now + timedelta(self.TOKEN_VALID_DAYS)
            )
            # commit()
        return True


class AbstractPermission(object):
    name = Required(str)
    granted = Set('PermissionGranted')


class AbstractPermissionGranted(object):
    user = Optional('User')
    group = Optional('Group')
    permission = Required('Permission')

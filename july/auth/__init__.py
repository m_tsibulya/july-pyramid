from .backends import EmailBackend


def login_user(backend, user, user_social_auth):
    EmailBackend.authenticate(backend.strategy.request, user)


def login_required(request):
    return getattr(request, 'user', None) is not None

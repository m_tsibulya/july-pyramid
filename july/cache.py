#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import json
import hashlib
from pyramid.httpexceptions import HTTPOk
from .conf import settings
from .utils import lazy


class Cache(object):
    PREFIX = 'cache'

    @lazy
    def r(self):
        pool = redis.ConnectionPool(
            host=settings['redis.sessions.host'],
            port=settings['redis.sessions.port'],
            password=settings['redis.sessions.password'],
            db=settings['redis.sessions.db']
        )
        return redis.Redis(connection_pool=pool)

    @staticmethod
    def convert(data):
        if isinstance(data, bytes): return data.decode('ascii')
        if isinstance(data, dict): return dict(map(Cache.convert, data.items()))
        if isinstance(data, tuple): return map(Cache.convert, data)
        return data

    def set(self, key, value, ex=None):
        self.r.set('%s:%s' % (self.PREFIX, key), value, ex=ex or settings['redis.sessions.expire'])

    def set_dict(self, key, value, ex=None):
        self.r.hmset('%s:%s' % (self.PREFIX, key), value)
        self.r.expire('%s:%s' % (self.PREFIX, key), ex or settings['redis.sessions.expire'])

    def get_dict(self, key):
        return Cache.convert(self.r.hgetall('%s:%s' % (self.PREFIX, key)))

    def get(self, key):
        return Cache.convert(self.r.get('%s:%s' % (self.PREFIX, key)))

    def __setitem__(self, key, value):
        self.set(key, value)

    def __getitem__(self, key):
        return self.get(key)

    def clean(self, *prefixes):
        prefix = ':'.join([self.PREFIX] + prefixes)
        for key in self.r.scan_iter("%s:*" % prefix):
            self.r.delete(key)


_cache = Cache()

def cache(ex):
    def real_cache(f):
        def wrapped_f(context, request, *args, **kwargs):
            use = request.method.upper() == 'GET'
            key = ''

            if use:
                h = hashlib.md5()
                h.update(f.__name__.encode('utf-8'))
                for k, v in request.matchdict.items():
                    h.update(k.decode('utf-8'))
                    h.update(v.decode('utf-8'))
                for k, v in request.GET.items():
                    h.update(k.decode('utf-8'))
                    h.update(v.decode('utf-8'))
                key = h.hexdigest()

            key2 = '%s:h' % key

            if use:
                data = _cache.get(key)
                headers = _cache.get(key2)
                if data and headers:
                    return {
                        'data': data,
                        'headers': json.loads(headers),
                        'status': HTTPOk.code
                    }

            resp = f(context, request, *args, **kwargs)
            if resp.get('status') == HTTPOk.code and use:
                _cache.set(key, resp['data'], ex)
                _cache.set(key2, json.dumps(resp['headers']), ex)
            return resp
        return wrapped_f
    return real_cache
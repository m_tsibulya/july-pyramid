import os
import errno


def prepare_path(filename):
    _dir = os.path.dirname(filename)
    if not os.path.exists(_dir):
        try:
            os.makedirs(_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

from pony.orm.dbproviders.mysql import MySQLStrConverter
from july.db.converters.file.converter import FileConverter


class MysqlFileConverter(MySQLStrConverter, FileConverter):
    pass

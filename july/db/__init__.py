"""
v 2017.7

pony orm support with some extra features support such as file fields
https://ponyorm.com/

Some hooks for autogeneration models from tables for use them in migrations

Currernt version support only single database operations
"""
import inspect
from datetime import datetime
from io import IOBase
from pony.orm import *
from pony.orm.core import Attribute

# from july.conf import settings
# from july.compat.datetime import datetime
# from .dbproviders.mysql import MysqlFileConverter
# from .converters.dt import DatetimeConverter

db = Database()


def connect(settings, create_tables=False):
    db.bind(
        settings['db.provider'],
        user=settings['db.user'],
        password=settings['db.password'],
        host=settings['db.host'],
        database=settings['db.name']
    )
    if settings['db.provider'] == 'mysql':
        from july.db.dbproviders.mysql import MysqlFileConverter
        db.provider.converter_classes.append((IOBase, MysqlFileConverter))

    db.generate_mapping(create_tables=create_tables)


def get_fields(klass):
    """

    :param klass:
    :return: list of tuples (name, field)
    """
    return inspect.getmembers(
        klass, lambda x: isinstance(x, (Set, Attribute))
    )


def check_set(key, object):
    chunks = key.split('.') if isinstance(key, unicode) else key
    return isinstance(getattr(object, chunks[0]), Set) or (chunks[1:] and check_set(chunks[1:], object))


def quote_value_for_eval(val):
    if isinstance(val, str):
        if val.endswith(','):
            return [quote_value_for_eval(x) for x in val.split(',')[:-1]]
        if val.isdigit():
            return int(val)
        try:
            return float(val)
        except ValueError:
            return '"%s"' % val
    return val


def build_from_abstract(name, classes, attrs):
    for _class in classes:
        attrs.update(get_fields(_class))
    return type(name, classes, attrs)

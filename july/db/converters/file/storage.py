import os
import uuid
import urllib
import shutil
import tempfile
from july.utils import lazy
# from july.conf import settings
from .file import WrappedStoredFile


def uuid_filename_function(filename):
    return str(uuid.uuid4())


def path_function(*args, **kwargs):
    return None


class Storage(object):
    def __init__(self, path=None,
                 filename_function=uuid_filename_function,
                 path_function=path_function, url='/media/',
                 host=None
                 ):
        self._path = path
        self._host = host
        self._url = url
        self.filename_getter = filename_function
        self.path_getter = path_function

    @lazy
    def path(self):
        from july.conf import settings
        return self._path or settings['storage.path']

    @lazy
    def host(self):
        from july.conf import settings
        return self._host or settings['storage.host']

    @lazy
    def url(self):
        if self.host:
            return urllib.parse.urljoin(self.host, self._url)
        return self._url

    def get_path(self, relative_path):
        chunks = [] if self.path.startswith(os.sep) else [os.getcwd()]
        chunks.extend([self.path, relative_path])
        return os.path.join(
            *filter(
                bool,
                chunks
            )
        )

storage = Storage()


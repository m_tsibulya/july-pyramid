import io
import urllib
import os
import shutil
from july.utils import lazy
from july.utils.paths import prepare_path


class WrappedStoredFile(object):
    def __init__(self, storage, path=None, filename=None, stream=None, file=None):
        self.storage = storage
        self.__filename__ = filename
        self.__path__ = path
        self.__file__ = io.BytesIO(stream or file.read()) if (stream or file) else file

    @lazy
    def file(self):
        return self.__file__ or open(os.path.join(self.storage.path, self.path), 'rb')

    @property
    def path(self):
        if not self.__path__:
            return None
        return self.__path__

    @lazy
    def filename(self):
        if self.__path__ :
            return os.path.basename(self.__path__)

        _filename, _extension = os.path.splitext(self.__filename__)
        return ''.join((self.storage.filename_getter(self.__filename__), _extension))

    @property
    def url(self):
        if not self.__path__:
            return None
        return urllib.parse.urljoin(self.storage.url, urllib.request.pathname2url(self.__path__))

    def __unicode__(self):
        return self.url

    def to_dict(self):
        return {
            'url': self.url,
            'path': self.path
        }

    def save(self, *args, **kwargs):
        if not self.__file__:
            return self

        sub_path = self.storage.path_getter(*args, **kwargs) if callable(self.storage.path_getter) else self.storage.path_getter

        self.__path__ = os.path.join(sub_path.strip(os.sep), self.filename) if sub_path else self.filename

        final_path = self.storage.get_path(self.__path__)

        temp_file_path = final_path + '.chunk'

        # TODO, create subdir if necessary

        prepare_path(temp_file_path)
        prepare_path(final_path)

        with open(temp_file_path, 'wb') as output_file:
            shutil.copyfileobj(self.__file__, output_file)

        os.rename(temp_file_path, final_path)
        return self

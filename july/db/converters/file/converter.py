from io import IOBase
from pony.orm.dbapiprovider import StrConverter
from .file import WrappedStoredFile
from .storage import Storage


class FileConverter(StrConverter):
    def init(self, kwargs):
        # Override this method to process additional positional
        # and keyword arguments of the attribute

        if self.attr is not None:
            # self.attr.args can be analyzed here
            self.args = self.attr.args

        self.storage = kwargs.pop("storage", Storage(
            
        ))
        self.filename = kwargs.pop("filename", None)
        upload_to = kwargs.pop("upload_to", None)
        if upload_to:
            self.storage.path_getter = upload_to
        # You should take all valid options from this kwargs
        # What is left in is regarded as unrecognized option

    def validate(self, val, obj):
        # convert value to the necessary type (e.g. from string)
        # validate all necessary constraints (e.g. min/max bounds)
        return val

    def py2sql(self, val):
        # prepare the value (if necessary) to storing in the database
        if isinstance(val, WrappedStoredFile):
            return val.save().path

        # if isinstance(val, IOBase):
        #     return self.storage.save(val, filename_function=self.filename)

        # if isinstance(val, dict):
        #     return val['path']

        return val

    def sql2py(self, val):
        # convert value (if necessary) after the reading from the db
        if val:
            return WrappedStoredFile(self.storage, path=val)
        return val

    def sql_type(self):
        # generate corresponding SQL type, based on attribute options
        return "SOME_SQL_TYPE_DEFINITION"


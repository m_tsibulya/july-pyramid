from pony.orm.dbapiprovider import StrConverter, DatetimeConverter as _DatetimeConverter
from july.compat.datetime import utc


class DatetimeConverter(_DatetimeConverter):
    def sql2py(self, val):
        return super(DatetimeConverter, self).sql2py(val) #.replace(tzinfo=utc)

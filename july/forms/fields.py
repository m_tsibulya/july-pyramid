from io import IOBase
from wtforms.fields import *
from wtforms.validators import DataRequired
from pony.orm.ormtypes import *
from july.compat.datetime import datetime
from pony.orm.core import Entity, Required, Optional, PrimaryKey


class Converter:
    """
    Not supported:
    time, timedelta
    """
    types = {
        str: StringField,
        unicode: StringField,
        Decimal: DecimalField,
        float: FloatField,
        int: IntegerField,
        datetime: DateTimeField,
        date: DateField,
        bool: BooleanField,
        buffer: FileField,
        bytes: FileField,
        IOBase: FileField,
        LongStr: TextField,
        LongUnicode: TextField,
        UUID: StringField,
        Json: TextField
    }

    @classmethod
    def convert(cls, field):
        py_type = field.py_type
        wtfield = cls.types.get(py_type)
        validators = []

        if isinstance(field, Required) and not isinstance(field, PrimaryKey) and \
                not getattr(field, 'default', None):
            validators.append(DataRequired())

        if isinstance(py_type, Entity):
            return IntegerField, validators

        if py_type in (
            time, timedelta
        ):
            return None, None

        return wtfield, validators
from pyramid.request import Request as __Request__
from july.utils import lazy


class LazyUser:
    is_staff = False
    is_superuser = False

    def __init__(self, request):
        self.request = request
        self.id = request.authenticated_userid
        for k, v in request.session.get('user_data', {}).items():
            setattr(self, k, v)

    @property
    def is_authenticated(self):
        return bool(self.request.authenticated_userid)

    def has_perm(self, *args, **kwargs):
        # TODO
        return False


class Request(__Request__):
    @lazy
    def user(self):
        return LazyUser(self)


